#!/usr/bin/env python3
#
# (C) Beuc 2019
import sys
import os, urllib.parse, html, io

# This function based on /usr/lib/python3.5/http/server.py
def list_directory(path):
    list = os.listdir(path)
    list.sort(key=lambda a: a.lower())
    r = []
    try:
        displaypath = urllib.parse.unquote(path,
                                           errors='surrogatepass')
    except UnicodeDecodeError:
        displaypath = urllib.parse.unquote(path)
    displaypath = html.escape(displaypath, quote=False)
    enc = sys.getfilesystemencoding()
    title = 'Directory listing for %s' % displaypath
    r.append('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" '
             '"http://www.w3.org/TR/html4/strict.dtd">')
    r.append('<html>\n<head>')
    r.append('<meta http-equiv="Content-Type" '
             'content="text/html; charset=%s">' % enc)
    r.append('<title>%s</title>\n</head>' % title)
    r.append('<body>\n<h1>%s</h1>' % title)
    header_path = os.path.join(path, 'HEADER.html');
    if os.path.exists(header_path):
        r.append(open(header_path, 'r').read())
    r.append('<hr>\n<ul>')
    for name in list:
        if name == 'index.html':
            continue
        fullname = os.path.join(path, name)
        displayname = linkname = name
        # Append / for directories or @ for symbolic links
        if os.path.isdir(fullname):
            displayname = name + "/"
            linkname = name + "/"
        if os.path.islink(fullname):
            displayname = name + "@"
            # Note: a link to a directory displays with @ and links with /
        r.append('<li><a href="%s">%s</a></li>'
                % (urllib.parse.quote(linkname,
                                      errors='surrogatepass'),
                   html.escape(displayname, quote=False)))
    r.append('</ul>\n<hr>\n</body>\n</html>\n')
    encoded = '\n'.join(r).encode(enc, 'surrogateescape')
    f = io.open(os.path.join(path, 'index.html'), 'wb')
    f.write(encoded)
    f.close()

for dir in sys.argv[1::]:
    if os.path.isdir(dir):
        print("Generating", os.path.join(dir, 'index.html'))
        list_directory(dir)
    else:
        print("Not a directory:", dir)
